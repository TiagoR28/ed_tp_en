/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import Colecoes.UnorderedList;
import Enums.Dificuldade;
import exceptions.ElementNotFoundException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import tp_casaassombrada.Aposento;
import tp_casaassombrada.GrafoMapa;
import tp_casaassombrada.LoadJson;
import tp_casaassombrada.Mapa;
import tp_casaassombrada.Player;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Game_On extends javax.swing.JFrame {

    private final GrafoMapa gm;
    private Aposento AposentoAtual;
    private Aposento Objectivo;
    private final Player user;
    private String passados;
    private final Inicio outroFrame;
    private final String caminho;

    /**
     * Creates new form Frame_1
     *
     * @param caminhoMapa
     * @param Inicial
     * @param Objectivo
     * @param outroFrame
     * @param dificuldade
     * @param user
     *
     * @throws java.io.FileNotFoundException
     * @throws org.json.simple.parser.ParseException
     * @throws exceptions.ElementNotFoundException
     */
    public Game_On(String caminhoMapa, String Inicial, String Objectivo, Inicio outroFrame, Dificuldade dificuldade, String user) throws FileNotFoundException, IOException, ParseException, ElementNotFoundException {
        initComponents();

        Object obj1 = new JSONParser().parse(new FileReader(caminhoMapa));
        JSONObject jsonObjct = (JSONObject) obj1;
        JSONArray jsonAposentos = (JSONArray) jsonObjct.get("mapa");

        //Lista que guarda todas as informações do documento Json
        UnorderedList<Aposento> lista = new UnorderedList<>();

        //Criação do User
        this.user = new Player(user, Integer.parseInt((String) jsonObjct.get("pontos").toString()), dificuldade);

        //Execução da class que guarda os valores recebidos pelo documento Json na Lista introduzida
        LoadJson jsonMapa = new LoadJson(lista, jsonAposentos);

        //Iniciar o Grafo
        this.gm = new GrafoMapa();

        //Criar o Grafo
        this.defenirGrafo(lista, Inicial, Objectivo);

        this.passados = this.AposentoAtual.getAposenAtual();
        this.Atual.setText(this.AposentoAtual.getAposenAtual());
        this.Vida.setText("Vida Atual: " + this.user.getVida());
        this.outroFrame = outroFrame;
        this.jTextArea1.setText(this.passados);
        this.jTextArea1.setEditable(false);
        this.outroFrame.setVisible(false);
        this.caminho = caminhoMapa;
    }

    /**
     *
     * metodo de contrucao do grafo para o modo manual
     *
     * @param lista
     * @param Inicial
     * @param Objectivo
     * @throws ElementNotFoundException
     */
    public final void defenirGrafo(UnorderedList<Aposento> lista, String Inicial, String Objectivo) throws ElementNotFoundException {
        //Valores que permitem aceder a informação da Lista
        Iterator iterator = lista.iterator();
        int count = 0;
        while (count < lista.size()) {
            Aposento apo = (Aposento) iterator.next();

            //Adição de Vertice ao grafo com um valor da Lista
            gm.addVertex(apo);
            count++;

            if (apo.getAposenAtual().equals(Inicial)) {
                this.AposentoAtual = apo;
            } else if (apo.getAposenAtual().equals(Objectivo)) {
                this.Objectivo = apo;
            }
        }

        //Adicionar as conequeções entre os Vertices do Grafo
        count = 0;
        Iterator itr = lista.iterator();

        while (count < lista.size()) {
            Aposento ligacao = (Aposento) itr.next();
            Iterator itr2 = ligacao.getLigacoes().iterator();

            while (itr2.hasNext()) {
                //Adição da Edge ao grafo pelos contactos
                Aposento ligacao2 = (Aposento) itr2.next();
                gm.addEdge(ligacao, ligacao2, ligacao2.getFantasma());
            }
            count++;
        }
    }

    /**
     * metodo iterador que percorre as possiveis ligacoes seguintes ao aposento
     * atual e torna-as visiveis para o jogador
     */
    public void quaisVisiveis() {
        Iterator iterator = this.AposentoAtual.getLigacoes().iterator();
        Aposento a;
        int list = this.AposentoAtual.getLigacoes().size();

        switch (list) {
            case 1:
                this.F2.setVisible(false);
                this.L2.setVisible(false);
                this.B2.setVisible(false);
            case 2:
                this.F3.setVisible(false);
                this.L3.setVisible(false);
                this.B3.setVisible(false);
            case 3:
                this.F4.setVisible(false);
                this.L4.setVisible(false);
                this.B4.setVisible(false);
            case 4:
                this.F5.setVisible(false);
                this.L5.setVisible(false);
                this.B5.setVisible(false);
            case 5:
                this.F6.setVisible(false);
                this.L6.setVisible(false);
                this.B6.setVisible(false);
            case 6:
                this.F7.setVisible(false);
                this.L7.setVisible(false);
                this.B7.setVisible(false);
            case 7:
                this.F8.setVisible(false);
                this.L8.setVisible(false);
                this.B8.setVisible(false);

            default:
                a = (Aposento) iterator.next();
                this.L1.setText(a.getAposenAtual());
                this.F1.setText("Fantasma: " + a.getFantasma());

                if (list > 1) {
                    a = (Aposento) iterator.next();
                    this.L2.setText(a.getAposenAtual());
                    this.F2.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 2) {
                    a = (Aposento) iterator.next();
                    this.L3.setText(a.getAposenAtual());
                    this.F3.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 3) {
                    a = (Aposento) iterator.next();
                    this.L4.setText(a.getAposenAtual());
                    this.F4.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 4) {
                    a = (Aposento) iterator.next();
                    this.L5.setText(a.getAposenAtual());
                    this.F5.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 5) {
                    a = (Aposento) iterator.next();
                    this.L6.setText(a.getAposenAtual());
                    this.F6.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 6) {
                    a = (Aposento) iterator.next();
                    this.L7.setText(a.getAposenAtual());
                    this.F7.setText("Fantasma: " + a.getFantasma());
                }
                if (list > 7) {
                    a = (Aposento) iterator.next();
                    this.L8.setText(a.getAposenAtual());
                    this.F8.setText("Fantasma: " + a.getFantasma());
                }
                break;
        }
    }

    public void setAllViseble() {
        this.F2.setVisible(true);
        this.L2.setVisible(true);
        this.B2.setVisible(true);
        this.F3.setVisible(true);
        this.L3.setVisible(true);
        this.B3.setVisible(true);
        this.F4.setVisible(true);
        this.L4.setVisible(true);
        this.B4.setVisible(true);
        this.F5.setVisible(true);
        this.L5.setVisible(true);
        this.B5.setVisible(true);
        this.F6.setVisible(true);
        this.L6.setVisible(true);
        this.B6.setVisible(true);
        this.F7.setVisible(true);
        this.L7.setVisible(true);
        this.B7.setVisible(true);
        this.F8.setVisible(true);
        this.L8.setVisible(true);
        this.B8.setVisible(true);
    }

    /**
     * Metodo iterador que guarda o caminho que o jogador escolheu e que em caso
     * de chegar ao objetivo guarda em ficheiro senao, mostra mensagem de erro
     * sem pontos restantes
     *
     * @param i
     */
    public void selecionado(int i) {
        Iterator iterator = this.AposentoAtual.getLigacoes().iterator();
        Aposento a = new Aposento();
        for (int y = 0; y < i; y++) {
            a = (Aposento) iterator.next();
        }

        //Caso tenha atingido o objectivo
        if (a.equals(this.Objectivo)) {
            try {
                this.passados = this.passados + "\n" + a.getAposenAtual();
                String[] array = this.passados.split("\n");
                String pass = array[0];
                Mapa m = new Mapa("Tiago", this.user.getVida());

                for (int l = 1; l < array.length; l++) {
                    pass = pass + " -> " + array[l];

                }
                m.setCaminho(pass);
                JOptionPane.showMessageDialog(null, "Sucesso: Acabou de passar o jogo com " + this.user.getVida() + "\n\n" + pass);

                LoadJson jsonMapa = new LoadJson();
                jsonMapa.save(m, this.user, "recursos/saves/SAVE Play-" + this.caminho.split("/")[this.caminho.split("/").length - 1]);

                this.outroFrame.setVisible(true);
                this.dispose();
            } catch (IOException | ParseException ex) {
                Logger.getLogger(Game_On.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            this.AposentoAtual = a;
            int dano = a.getFantasma() * user.getGrau().multiplicador();
            this.user.setVida(this.user.getVida() - dano);

            //Caso tenha Morrido
            if (this.user.getVida() <= 0) {
                JOptionPane.showMessageDialog(null, "Fail: Acabou de Perder\n\nUltimo Dano: " + dano);
                this.outroFrame.setVisible(true);
                this.dispose();
            }

            this.passados = this.passados + "\n" + a.getAposenAtual();
            this.Atual.setText(this.AposentoAtual.getAposenAtual());
            this.Vida.setText("Vida Atual: " + this.user.getVida());
            this.jTextArea1.setText(this.passados);
            this.setAllViseble();
            this.quaisVisiveis();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Atual = new javax.swing.JLabel();
        Vida = new javax.swing.JLabel();
        L3 = new javax.swing.JLabel();
        F3 = new javax.swing.JLabel();
        B3 = new javax.swing.JButton();
        B1 = new javax.swing.JButton();
        L1 = new javax.swing.JLabel();
        F1 = new javax.swing.JLabel();
        B2 = new javax.swing.JButton();
        L2 = new javax.swing.JLabel();
        F2 = new javax.swing.JLabel();
        B4 = new javax.swing.JButton();
        L4 = new javax.swing.JLabel();
        F4 = new javax.swing.JLabel();
        B7 = new javax.swing.JButton();
        L8 = new javax.swing.JLabel();
        B5 = new javax.swing.JButton();
        F8 = new javax.swing.JLabel();
        L5 = new javax.swing.JLabel();
        F5 = new javax.swing.JLabel();
        B6 = new javax.swing.JButton();
        L7 = new javax.swing.JLabel();
        L6 = new javax.swing.JLabel();
        F7 = new javax.swing.JLabel();
        F6 = new javax.swing.JLabel();
        B8 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Salas Passadas");

        jLabel2.setText("Localização Atual");

        Atual.setText("jLabel3");

        Vida.setText("Vida Atual: ");

        L3.setText("jLabel4");

        F3.setText("Fantasma: ");

        B3.setText("Seguir");
        B3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B3ActionPerformed(evt);
            }
        });

        B1.setText("Seguir");
        B1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B1ActionPerformed(evt);
            }
        });

        L1.setText("jLabel4");

        F1.setText("Fantasma: ");

        B2.setText("Seguir");
        B2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B2ActionPerformed(evt);
            }
        });

        L2.setText("jLabel4");

        F2.setText("Fantasma: ");

        B4.setText("Seguir");
        B4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B4ActionPerformed(evt);
            }
        });

        L4.setText("jLabel4");

        F4.setText("Fantasma: ");

        B7.setText("Seguir");
        B7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B7ActionPerformed(evt);
            }
        });

        L8.setText("jLabel4");

        B5.setText("Seguir");
        B5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B5ActionPerformed(evt);
            }
        });

        F8.setText("Fantasma: ");

        L5.setText("jLabel4");

        F5.setText("Fantasma: ");

        B6.setText("Seguir");
        B6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B6ActionPerformed(evt);
            }
        });

        L7.setText("jLabel4");

        L6.setText("jLabel4");

        F7.setText("Fantasma: ");

        F6.setText("Fantasma: ");

        B8.setText("Seguir");
        B8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B8ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(78, 78, 78)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Atual)
                            .addComponent(jLabel2)
                            .addComponent(Vida))
                        .addGap(68, 68, 68)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(F2)
                            .addComponent(L2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(F1)
                                .addComponent(L1))
                            .addComponent(F3)
                            .addComponent(L3)
                            .addComponent(F4)
                            .addComponent(L4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(B3)
                            .addComponent(B1)
                            .addComponent(B2)
                            .addComponent(B4))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(F8)
                            .addComponent(L8)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(F6)
                                .addComponent(L6))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(F5)
                                .addComponent(L5))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(F7)
                                .addComponent(L7)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(B8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(B6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(B5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(B7, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel1)))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(L3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(F3))
                            .addComponent(B3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(L7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(F7))
                            .addComponent(B7))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(Atual)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(Vida))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(B1)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(L1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(F1)))
                                        .addGap(18, 18, 18)
                                        .addComponent(L2)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(F2))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(L5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(F5))
                                    .addComponent(B5))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(B2)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(L6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(F6))
                                    .addComponent(B6))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(L4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(F4))
                            .addComponent(B4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(L8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(F8))
                                .addComponent(B8, javax.swing.GroupLayout.Alignment.TRAILING)))
                        .addContainerGap(43, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void B1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B1ActionPerformed
        this.selecionado(1);
    }//GEN-LAST:event_B1ActionPerformed

    private void B2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B2ActionPerformed
        this.selecionado(2);
    }//GEN-LAST:event_B2ActionPerformed

    private void B3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B3ActionPerformed
        this.selecionado(3);
    }//GEN-LAST:event_B3ActionPerformed

    private void B4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B4ActionPerformed
        this.selecionado(4);
    }//GEN-LAST:event_B4ActionPerformed

    private void B5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B5ActionPerformed
        this.selecionado(5);
    }//GEN-LAST:event_B5ActionPerformed

    private void B6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B6ActionPerformed
        this.selecionado(6);
    }//GEN-LAST:event_B6ActionPerformed

    private void B7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B7ActionPerformed
        this.selecionado(7);
    }//GEN-LAST:event_B7ActionPerformed

    private void B8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B8ActionPerformed
        this.selecionado(8);
    }//GEN-LAST:event_B8ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Atual;
    private javax.swing.JButton B1;
    private javax.swing.JButton B2;
    private javax.swing.JButton B3;
    private javax.swing.JButton B4;
    private javax.swing.JButton B5;
    private javax.swing.JButton B6;
    private javax.swing.JButton B7;
    private javax.swing.JButton B8;
    private javax.swing.JLabel F1;
    private javax.swing.JLabel F2;
    private javax.swing.JLabel F3;
    private javax.swing.JLabel F4;
    private javax.swing.JLabel F5;
    private javax.swing.JLabel F6;
    private javax.swing.JLabel F7;
    private javax.swing.JLabel F8;
    private javax.swing.JLabel L1;
    private javax.swing.JLabel L2;
    private javax.swing.JLabel L3;
    private javax.swing.JLabel L4;
    private javax.swing.JLabel L5;
    private javax.swing.JLabel L6;
    private javax.swing.JLabel L7;
    private javax.swing.JLabel L8;
    private javax.swing.JLabel Vida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
