/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

/**
 *
 * @author TIAGOROBERTOMARQUESP
 */
public enum Dificuldade {
    BASICO,
    NORMAL,
    DIFICIL;

    public int multiplicador() {
        switch (this) {
            case BASICO:
                return 1;
            case NORMAL:
                return 2;
            case DIFICIL:
                return 3;
            default:
                return 1;
        }

    }

    public Dificuldade determinar(int i) {
        switch (i) {
            case 1:
                return Dificuldade.BASICO;
            case 2:
                return Dificuldade.NORMAL;
            case 3:
                return Dificuldade.DIFICIL;
            default:
                return Dificuldade.BASICO;
        }
    }
}
