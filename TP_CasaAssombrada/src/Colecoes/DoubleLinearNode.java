/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecoes;

import java.util.Objects;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class DoubleLinearNode<T> {
    
    /** reference to next node in list */
    private DoubleLinearNode<T> next;
    /** reference to previous node in list */
    private DoubleLinearNode<T> previous;
    /** element stored at this node */
    private T element;
    /**Creates an empty node.*/
    public DoubleLinearNode() {
        next = null;
        previous = null;
        element = null;
    }
    /**
    * Creates a node storing the specified element.
    * @param elem element to be stored */
    public DoubleLinearNode(T elem) {
        next = null;
        previous = null;
        element = elem;
    }
    /**
    * Returns the node that follows this one.
    * @return DoubleLinearNode<T> reference to next node*/
    public DoubleLinearNode<T> getNext() {
        return next;
    }
    /**
    * Sets the node that follows this one.
    * @param node node to follow this one*/
    public void setNext(DoubleLinearNode<T> node) {
        next = node;
    }
    
    public DoubleLinearNode<T> getPrevious() {
        return previous;
    }

    public void setPrevious(DoubleLinearNode<T> previous) {
        this.previous = previous;
    }
    /**
    * Returns the element stored in this node.
    * @return T element stored at this node*/
    public T getElement() {
        return element;
    }
    /**
    * Sets the element stored in this node.
    * @param elem element to be stored at this node*/
    public void setElement(T elem) {
        element = elem;
    }

    @Override
    public String toString() {
        return "LinearNode{" + "element=" + element.toString() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.element);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DoubleLinearNode<?> other = (DoubleLinearNode<?>) obj;
        if (!Objects.equals(this.element, other.element)) {
            return false;
        }
        return true;
    }
}
