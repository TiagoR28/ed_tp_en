
package Colecoes;

import exceptions.EmptyCollectionException;


/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Stack<T>{
    

    private final int DEFAULT_CAPACITY = 100;
    private int top;
    private T[] stack;

    /**
     * Método construtor que permite instanciar uma stack vazia.({@link Stack})
     */
    public Stack() { 
        top = 0;
        stack = (T[])(new Object[DEFAULT_CAPACITY]);
    }

    /**
     * Método construtor que permite instanciar uma stack vazia.({@link Stack})
     * @param initialCapacity
     */
    public Stack (int initialCapacity){
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }

    /**
     * Método que adiciona elemento à stack.({@link Stack})
     * @param t
     */
    public void push(Object t) {
        if (size() == stack.length)
            expandCapacity();
        stack[top] = (T) t;
        top++;
    }

    /**
     * Método que remove elemento à stack.({@link Stack})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */
    public T pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException();
        top--;
        T result = stack[top];
        stack[top] = null;
        return result;
    }

    /**
     * Método que devolve o top da stack.({@link Stack})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */
    public Object peek() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException();
        return stack[top-1];
    }

    /**
     * Método que verifica se a stack está vazia.({@link Stack})
     * @return 
     */
    public boolean isEmpty() {
        if(top != 0){
            return false;
        }
        return true;
    }

    /**
     * Método que devolve o tamanho da stack.({@link Stack})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */
    public int size() {
        return top;
    }

    /**
     * Método que aumenta o tamanho stack.({@link Stack})
     */   
    public void expandCapacity(){
        
        T[] stack_tmp = (T[])(new Object[top + 1]);
        for (int i=0; i<= top; i++){
            
            stack_tmp[i] = stack[i];
        }
        this.stack = stack_tmp;
        top++;
    }
}
