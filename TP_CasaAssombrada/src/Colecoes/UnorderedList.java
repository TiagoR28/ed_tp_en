
package Colecoes;

import exceptions.ElementNotFoundException;


/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class UnorderedList<T> extends ArrayList<T>{
   
    /**
     * Metodo construtor que instanceia a classe.({@link OrderedList})
     */
    public UnorderedList() {
        super();
    }
   
    /**
     * Metodo que adiciona elemento á frente na lista.({@link OrderedList})
     * @param t
     */
    public void addToFront(T t) {
        if(isFull()){
        expandCapacity();
        }
        
        for (int i = last; i > 0; --i) {
            list[i] = list[i - 1];
        }

        list[0] = t;
        ++last;
    }
   
    /**
     * Metodo que adiciona elemento atrás na lista.({@link OrderedList})
     * @param t
     */
    public void addToRear(T t) {
        if(isFull()){
        expandCapacity();
        }

        if (size() == list.length) {
            expandCapacity();
        }

        list[last] = t;
        ++last;
    }
   
    /**
     * Metodo que adiciona elemento depois de um certo elemento lista.({@link OrderedList})
     * @param t
     * @param t1
     * @throws exceptions.ElementNotFoundException
     */
    public void addAfter(T t, T t1) throws ElementNotFoundException {
        if(isFull()){
        expandCapacity();
        }
        
        int i = 0;

        while (i < last && !t1.equals(list[i])) {
            ++i;
        }

        if (i == last) {
            throw new ElementNotFoundException();
        }
        ++i;
        for (int k = last; k > i; --k) {
            list[k] = list[k - 1];
        }

        list[i] = t;
        ++last;
    }
}