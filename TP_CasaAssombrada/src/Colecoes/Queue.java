
package Colecoes;
import exceptions.EmptyCollectionException;


/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Queue<T>{

    private LinearNode head;
    private LinearNode rear;
    private LinearNode queue;
    private int count;

    /**
     * Método construtor que permite instanciar uma queue vazia.({@link Queue})
     */
    public Queue() {

        this.head = null;
        this.rear = null;
        this.queue = null;
        this.count = 0;

    }

    /**
     * Metodo construtor que permite instanciar uma Queue com um elemento.({@link Queue})
     *
     * @param elem elemento que inicializa a Queue.
     */
    public Queue(T elem) {
        this.queue = new LinearNode(elem);
        this.head = this.queue;
        this.rear = this.queue;
        this.count = 1;

    }

    /**
     * Metodo que permite adicionar um elemento a queue.({@link Queue})
     *
     * @param element elemento que vai ser adicionado a rear.
     */
    public void enqueue(T element) {

        try {
            this.rear.setNext(new LinearNode(element));
            this.rear = this.rear.getNext();
        } catch (java.lang.NullPointerException e) {
            this.queue = new LinearNode(element);
            this.head = this.queue;
            this.rear = this.queue;
        }
        this.count++;
    }

    /**
     * Metodo que devolve o elemento no topo da Queue e o apaga da mesma.({@link Queue})
     *
     * @return elemento na head.
     * @throws exceptions.EmptyCollectionException
     */
    public T dequeue() throws EmptyCollectionException {

        LinearNode dev = this.head;
        this.head = this.head.getNext();
        this.count--;
        if(this.size() == 0){
            this.head = null;
            this.rear = null;
            this.queue = null;
        }
        return (T) dev.getElement();

    }

    /**
     * metodo que devolve o primeiro elemento.({@link Queue})
     *
     * @return primeiro elemento da queue.
     */
    public T first() {
        return (T) this.head.getElement();
    }

    /**
     * Verifica se a queue está vazia.({@link Queue})
     *
     * @return true se a que estiver vazia.
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Devolve o tamanho da queue.({@link Queue})
     *
     * @return queue size.
     */
    public int size() {
        return this.count;
    }

    /**
     * Metodo toString da queue.({@link Queue})
     * @return 
     */
    @Override
    public String toString() {
        String dev = "";
        LinearNode bus = this.queue;
        for (int i = 0; i < this.count; i++) {
            dev = dev + " " + bus.getElement().toString();
            bus = bus.getNext();
        }
        return dev;
    }
    
}
