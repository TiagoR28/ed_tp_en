
package Colecoes;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class LinearNode<T> {

    private LinearNode<T> next;
    private T element;
   
    /**
     * Metodo construtor que instanceia uma node vazia.({@link LinearNode})
     */
    public LinearNode() {
        this.element = null;
        this.next = null;
    }
   
    /**
     * Metodo construtor que instanceia uma node com um elemento expecifico.({@link LinearNode})
     * @param elem
     */
    public LinearNode(T elem) {
        this.next = null;
        this.element = elem;
    }

    /**
     * Método setter do next node.({@link LinearNode})
     *
     * @param node
     */
    public void setNext(LinearNode<T> node) {
        this.next = node;
    }

    /**
     * Método getter do next.({@link LinearNode})
     *
     * @return next
     */
    public LinearNode<T> getNext() {
        return this.next;
    }

    /**
     * Método setter do element.({@link LinearNode})
     *
     * @param elem
     */
    public void setElement(T elem) {
        this.element = elem;
    }

    /**
     * Método getter do element.({@link LinearNode})
     *
     * @return element
     */
    public T getElement() {
        return this.element;
    }
}
