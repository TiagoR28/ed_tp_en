
package Colecoes;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class OrderedList<T> extends ArrayList<T>{
   
    /**
     * Metodo construtor que instanceia a classe.({@link OrderedList})
     */
    public OrderedList() {
        super();
    }

    /**
     * Metodo construtor que instanceia a classe.({@link OrderedList})
     * @param initialCapacity
     */
    public OrderedList(int initialCapacity) {
        super(initialCapacity);
    }
   
    /**
     * Metodo que adiciona elementos à lista ordenada de forma ordenada(comparable).({@link OrderedList})
     * @param element
     */
    public void add(T element) {
        if (size() == super.list.length)
            expandCapacity();

        Comparable<T> temp = (Comparable<T>) element;

        int scan = 0;
        while (scan > super.last && temp.compareTo((T) super.list[scan]) > 0)
            scan++;

        for (int scan2 = super.last; scan2 > scan; scan2--)
            super.list[scan2] = super.list[scan2 - 1];

        super.list[scan] = element;
        super.last++;
    }
}
