
package Colecoes;

import java.util.Iterator;
import exceptions.ElementNotFoundException;
import exceptions.EmptyCollectionException;
/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Graph<T>{
    
    protected final int DEFAULT_CAPACITY = 20;
    protected int numVertices; 
    protected boolean[][] adjMatrix; 
    protected T[] vertices; 

    
    /**
     * Metodo construtor que instanceia a classe.({@link Graph})
     */
    public Graph() {
        numVertices = 0;
        this.adjMatrix = new boolean[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }
    
    /**
     * Metodo para adicionar vertices ao grafo.({@link Graph})
     * @param t
     */
    public void addVertex(T t) {
        if (numVertices == vertices.length) {
            expandCapacity();
        }
        vertices[numVertices] = t;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = false;
            adjMatrix[i][numVertices] = false;
        }
        numVertices++;
    }
    
    /**
     * Metodo para remover vertice ao grafo.({@link Graph})
     * @param t
     * @throws exceptions.ElementNotFoundException
     */
    public void removeVertex(T t) throws ElementNotFoundException {
        removeVertex(getIndex(t));
    }
        
    /**
     * Metodo para remover vertice ao grafo.({@link Graph})
     * @param index
     */
    public void removeVertex(int index) {
        if (indexIsValid(index)) {
            numVertices--;
            for (int i = index; i < numVertices; i++) {
                vertices[i] = vertices[i + 1];
            }
            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j <= numVertices; j++) {
                    adjMatrix[i][j] = adjMatrix[i + 1][j];
                }
            }
            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    adjMatrix[j][i] = adjMatrix[j][i + 1];
                }
            }
        }
    }

        
    /**
     * Metodo para adiciona ligações ao grafo.({@link Graph})
     * @param t
     * @param t1
     * @throws exceptions.ElementNotFoundException
     */
    public void addEdge(T t, T t1) throws ElementNotFoundException {
        addEdge(getIndex(t), getIndex(t1));
    }
        
    /**
     * Metodo para adiciona ligações ao grafo.({@link Graph})
     * @param index1
     * @param index2
     */    
    public void addEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = true;
            adjMatrix[index2][index1] = true;
        }
    }
        
    /**
     * Metodo que verifica se o indice é valido.({@link Graph})
     * @param index1
     * @return 
     */       
    protected boolean indexIsValid(int index1) {
        return index1 < vertices.length && index1 >= 0;
    }

    /**
     * Método getter do Index.({@link Graph})
     *
     * @param vertex1
     * @return
     */
    protected int getIndex(T vertex1) {
        for (int i = 0; i < numVertices; i++) {
            if (vertices[i].equals(vertex1)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Método que aumenta o tamanho de grafo.({@link Graph})
     */   
    protected void expandCapacity() {
        T[] tempVertices = (T[]) (new Object[vertices.length + 1]);
        boolean[][] tempAdjMatrix = new boolean[vertices.length + 1][vertices.length + 1];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                tempAdjMatrix[i][j] = adjMatrix[i][j];
            }
            tempVertices[i] = vertices[i];
        }
        vertices = tempVertices;
        adjMatrix = tempAdjMatrix;
    }

    /**
     * Método que remove ligações ao grafo.({@link Graph})
     * @param t
     * @param t1
     * @throws exceptions.ElementNotFoundException
     */ 
    public void removeEdge(T t, T t1) throws ElementNotFoundException {
        int index1 = getIndex(t);
        int index2 = getIndex(t1);
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = false;
            adjMatrix[index2][index1] = false;
        }
    }


    /**
     * Metodo iteratorBFS para percorrer o grafo({@link Graph})
     * @param t
     * @return 
     * @throws exceptions.EmptyCollectionException 
     */      
    public Iterator iteratorBFS(T t) throws EmptyCollectionException {
        Integer x = -1;
        int startIndex = getIndex(t);
        Queue<Integer> traversalQueue = new Queue<>();
        UnorderedList<T> resultList = new UnorderedList<>();
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToFront(vertices[x]);
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[x][i] && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Metodo iteratorDFS para percorrer o grafo({@link Graph})
     * @param t
     * @return 
     * @throws exceptions.EmptyCollectionException 
     */ 
    public Iterator iteratorDFS(T t) throws EmptyCollectionException {
        Integer x = -1;
        boolean found;
        int startIndex = getIndex(t);
        Stack<Integer> traversalStack = new Stack<>();
        UnorderedList<T> resultList = new UnorderedList<>();
        boolean[] visited = new boolean[numVertices];
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(startIndex);
        resultList.addToFront(vertices[startIndex]);
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = (Integer) traversalStack.peek();
            found = false;
            for (int i = 0; (i < numVertices) && !found; i++) {
                if (adjMatrix[x][i] && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addToFront(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        return resultList.iterator();
    }

    /**
     * Metodo iteratorShortestPath para percorrer o grafo pelo caminho mais curto({@link Graph})
     * @param t
     * @param t1
     * @return 
     * @throws exceptions.EmptyCollectionException 
     */   
    public Iterator iteratorShortestPath(T t, T t1) throws EmptyCollectionException {
        
        UnorderedList resultList = new UnorderedList<>();
        int startIndex = getIndex(t);
        int targetIndex = getIndex(t1);
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);
        while (it.hasNext()) {
            resultList.addToFront(vertices[(it.next())]);
        }
        return resultList.iterator();
    }

    /**
     * Metodo iteratorShortestPath para percorrer o grafo pelo caminho mais curto({@link Graph})
     * @param startIndex
     * @param targetIndex
     * @return 
     * @throws exceptions.EmptyCollectionException 
     */       
    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) throws EmptyCollectionException {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        Queue<Integer> traversalQueue = new Queue<>();
        UnorderedList<Integer> resultList = new UnorderedList<>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)
                || (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            index = (traversalQueue.dequeue());

            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[index][i] && !visited[i]) {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex)
        {
            return resultList.iterator();
        }

        Stack<Integer> stack = new Stack<>();
        index = targetIndex;
        stack.push(index);
        do {
            index = predecessor[index];
            stack.push(index);
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToFront((stack.pop()));
        }

        return resultList.iterator();
    
    }

    /**
     * Metodo que verifica se o grafo está vazio({@link Graph})
     * @return 
     */       
    public boolean isEmpty() {
        return numVertices == 0;
    }

    /**
     * Metodo que verifica se o grafo está conectado({@link Graph})
     * @return 
     */     
    public boolean isConnected() {
        for(int i = 0; i < numVertices; i++){
            for(int j = 0; j < numVertices; j++){
                if(adjMatrix[i][j] == false){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Metodo que devolve o tamanho do grafo({@link Graph})
     * @return 
     */     
    public int size() {
        return numVertices;
    }

    /**
     * Método getter do vertices.({@link Graph})
     *
     * @return vertices
     */
    public T[] getVertices() {
      return  this.vertices;
    }

    
}
