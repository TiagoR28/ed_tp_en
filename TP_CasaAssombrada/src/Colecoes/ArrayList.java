
package Colecoes;

import java.util.Iterator;
import exceptions.ElementNotFoundException;
import exceptions.EmptyCollectionException;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public abstract class ArrayList<T>{
   
    private final int DEFAULT_CAPACITY = 15;
    private int first;
    protected int last;
    private int position;
    protected T[] list;
    private int nElements;
    
    /**
     * Metodo construtor que instanceia a classe.({@link ArrayList})
     */
    public ArrayList() {
        this.first = 0;
        this.last = 0;
        this.position = -1;
        this.list = (T[]) new Object[this.DEFAULT_CAPACITY];
        this.nElements = 0;
    }
    
    /**
     * Metodo construtor que instanceia a classe.({@link ArrayList})
     * @param initialCapacity
     */    
    public ArrayList(int initialCapacity) {
        this.first = 0;
        this.last = 0;
        this.position = -1;
        this.list = (T[]) new Object[initialCapacity];
        this.nElements = 0;
    }
    
    /**
     * Metodo para remover o primeiro elemento da lista.({@link ArrayList})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */    
    public T removeFirst() throws EmptyCollectionException {
        T result;

        if (isEmpty()) {
            throw new EmptyCollectionException();
        } else {
            result = this.list[0];
            --(this.last);
            for (int i = 0; i < this.last; ++i) {
                this.list[this.last] = this.list[i + 1];
            }

            this.list[this.last] = null;
        }
        return result;
    }
    
    /**
     * Metodo para remover o ultimo elemento da lista.({@link ArrayList})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */   
    public T removeLast() throws EmptyCollectionException {
        T result;

        if (isEmpty()) {
            throw new EmptyCollectionException();
        } else {
            --(this.last);
            result = this.list[this.last];
            this.list[this.last] = null;
        }
        return result;
    }
    
    /**
     * Metodo para remover um elemento expecifico da lista.({@link ArrayList})
     * @param element
     * @return 
     * @throws exceptions.EmptyCollectionException
     * @throws exceptions.ElementNotFoundException
     */  
    public T remove(T element) throws EmptyCollectionException, ElementNotFoundException {
        T result;

        if (!contains(element)) {
            throw new ElementNotFoundException();
        } else {
            result = this.list[this.position];
            for(int i=this.position; i<this.last; i++){
                if(i+1 < this.last){
                    this.list[i] = this.list[i + 1];
                }
            }
            --(this.last);
            this.list[this.last] = null;
        }
        return result;
    }
    
    /**
     * Metodo que devolve o primeiro elemento da lista.({@link ArrayList})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */ 
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        } else {
            return this.list[this.first];
        }
    }
    
    /**
     * Metodo que devolve o ultimo elemento da lista.({@link ArrayList})
     * @return 
     * @throws exceptions.EmptyCollectionException
     */ 
    public T last() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        } else {
            return this.list[this.last - 1];
        }
    }
    
    /**
     * Metodo que verifica se o elemento existe na lista.({@link ArrayList})
     * @param target
     * @return 
     */
    public boolean contains(T target) {
        boolean contain = false;
        int i = 0;

        if (!isEmpty()) {
            while (!contain && i < this.last) {
                if (target.equals(this.list[i])) {
                    contain = true;
                    this.position = i;
                } else {
                    ++i;
                }
            }
        }
        return contain;
    }

    public boolean isEmpty() {
        
        return size() == 0;
    }
    
    /**
     * Metodo que devolve o tamanho da lista.({@link ArrayList})
     * @return 
     */
    public int size() {
        int count = 0;

        for (int i = 0; i < this.list.length; ++i) {
            if (this.list[i] != null) {
                ++count;
            }
        }
        return count;
    }
    
    /**
     * Iterador da lista.({@link ArrayList})
     * @return 
     */
    public Iterator<T> iterator() {
        BasicIterator<T> bi = new BasicIterator<>();
        return bi;
    }
    
    /**
     * Metodo que verifica se a lista está cheia.({@link ArrayList})
     * @return 
     */  
    public boolean isFull(){
        int count = this.last;
        return count == this.list.length;
    }
    
    /**
     * Metodo que aumenta o tamanho da lista.({@link ArrayList})
     */      
    public void expandCapacity(){
        T[] temp = (T[]) (new Object[this.list.length + 1]);

        for (int i = 0; i < this.list.length; i++) {
            temp[i] = this.list[i];
        }
        this.list = temp;
        this.last = temp.length - 1;
    }

    /**
     * Método getter do listSize.({@link ArrayList})
     *
     * @return listSize
     */
    public int getlistSize() {
        return this.list.length;
    }

    /**
     * Método getter do list.({@link ArrayList})
     *
     * @return list
     */
    public T[] getList() {
        return list;
    }

    /**
     * Método setter do list.({@link ArrayList})
     *
     * @param list
     */
    public void setList(T[] list) {
        this.list = list;
    }

    /**
     * Método getter do first.({@link ArrayList})
     *
     * @return first
     */
    public int getFirst() {
        return first;
    }

    /**
     * Método setter do first.({@link ArrayList})
     *
     * @param first
     */
    public void setFirst(int first) {
        this.first = first;
    }

    /**
     * Método getter do last.({@link ArrayList})
     *
     * @return last
     */
    public int getLast() {
        return last;
    }

    /**
     * Método setter do last.({@link ArrayList})
     *
     * @param last
     */
    public void setLast(int last) {
        this.last = last;
    }

    /**
     * Método getter do nElements.({@link ArrayList})
     *
     * @return nElements
     */
    public int getnElements() {
        return nElements;
    }

    /**
     * Método setter do nElements.({@link ArrayList})
     *
     * @param nElements
     */
    public void setnElements(int nElements) {
        this.nElements = nElements;
    } 
   
    /**
     * Classe do iterator.({@link ArrayList})
     *
     * @param <T>
     */ 
    public class BasicIterator<T> implements Iterator<T>{
        private int cursor;

    
        /**
        * Metodo construtor que instanceia a classe.({@link BasicIterator})
        */
        public BasicIterator(){
            cursor = 0;
        }

    
        /**
        * Metodo que percorre o iterator.({@link BasicIterator})
        */
        @Override
        public boolean hasNext() {
           
            if(cursor < ArrayList.this.getList().length && ArrayList.this.getList()[cursor] != null){
                return this.cursor < ArrayList.this.getLast();
            }
            return false;
        }
    
        /**
        * Metodo que devolve o objeto onde se encontra o iterator.({@link BasicIterator})
        */
        @Override
        public T next() {
            cursor++;
            return (T) ArrayList.this.getList()[cursor-1];
        }

        /**
         * Método getter do cursor.({@link BasicIterator})
        *
        * @return cursor
        */
        public int getCursor() {
            return cursor;
        }

        /**
        * Método setter do cursor.({@link BasicIterator})
        *
        * @param cursor
        */
        public void setCursor(int cursor) {
            this.cursor = cursor;
        }
    }
}

    
    

