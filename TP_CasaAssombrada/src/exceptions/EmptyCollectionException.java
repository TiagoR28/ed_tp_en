
package exceptions;

/**
 *
 * @author Tiago Risca 8150233
 * @author António Alves 8150052
 */
public class EmptyCollectionException extends Exception{
        
    /**
     * Metodo construtor que instanceia a classe, caso exceção lançar mensagem.({@link EmptyCollectionException})
     */
    public EmptyCollectionException(){
        super("Coleção vazia");
    }
}

