/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Enums.Dificuldade;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
class Historico implements Comparable<Historico> {

    private String jogador;
    private String mapa;
    private Dificuldade dificuldade;
    private int pontosRestantes;
    private String caminho;

    /**
     * metodo construtor que instancia a classe
     *
     * @param jogador
     * @param mapa
     * @param dificuldade
     * @param pontosRestantes
     * @param caminho
     */
    public Historico(String jogador, String mapa, Dificuldade dificuldade, int pontosRestantes, String caminho) {
        this.jogador = jogador;
        this.mapa = mapa;
        this.dificuldade = dificuldade;
        this.pontosRestantes = pontosRestantes;
        this.caminho = caminho;
    }

    /**
     * metodo getter de jogador
     *
     * @return
     */
    public String getJogador() {
        return jogador;
    }

    /**
     * metodo setter de jogador
     *
     * @return
     */
    public void setJogador(String jogador) {
        this.jogador = jogador;
    }

    /**
     * metodo getter de mapa
     *
     * @return
     */
    public String getMapa() {
        return mapa;
    }

    /**
     * metodo setter de mapa
     *
     * @return
     */
    public void setMapa(String mapa) {
        this.mapa = mapa;
    }

    /**
     * metodo getter de dificuldade
     *
     * @return
     */
    public Dificuldade getDificuldade() {
        return dificuldade;
    }

    /**
     * metodo setter de dificuldade
     *
     * @return
     */
    public void setDificuldade(Dificuldade dificuldade) {
        this.dificuldade = dificuldade;
    }

    /**
     * metodo getter de pontos restantes
     *
     * @return
     */
    public int getPontosRestantes() {
        return pontosRestantes;
    }

    /**
     * metodo setter de pontos restantes
     *
     * @return
     */
    public void setPontosRestantes(int pontosRestantes) {
        this.pontosRestantes = pontosRestantes;
    }

    /**
     * metodo getter de caminho
     *
     * @return
     */
    public String getCaminho() {
        return caminho;
    }

    /**
     * metodo setter de caminho
     *
     * @return
     */
    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    /**
     * Método compareTo para comparar os pontos restantes do historico. compara
     * a dificuldade e pontos
     */
    @Override
    public int compareTo(Historico o) {

        if (this.getDificuldade().multiplicador() < o.getDificuldade().multiplicador()) {
            return 1;
        } else if (this.getDificuldade().multiplicador() == o.getDificuldade().multiplicador()) {
            if (o.pontosRestantes == this.pontosRestantes) {
                return 0;
            } else if (this.pontosRestantes < o.pontosRestantes) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return -1;
        }

    }
}
