/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Enums.Dificuldade;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Player implements Comparable<Player> {

    private String nome;
    private int vida;
    private Dificuldade grau;

    /**
     * Metodo construtor para inicializar os pontos de vida do jogador
     *
     * @param nome
     * @param vida
     * @param grau
     */
    public Player(String nome, int vida, Dificuldade grau) {
        this.nome = nome;
        this.vida = vida;
        this.grau = grau;
    }

    /**
     * Método getter nome.
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método setter nome.
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Método getter vida.
     *
     * @return
     */
    public int getVida() {
        return vida;
    }

    /**
     * Método setter vida.
     *
     * @param vida
     */
    public void setVida(int vida) {
        this.vida = vida;
    }

    /**
     * Método getter do grau de dificuldade.
     *
     * @return
     */
    public Dificuldade getGrau() {
        return grau;
    }

    /**
     * Método setter do grau de dificuldade.
     *
     * @param grau
     */
    public void setGrau(Dificuldade grau) {
        this.grau = grau;
    }

    @Override
    public String toString() {
        return "Player{" + "nome=" + nome + ", score=" + vida + ", grau=" + grau + '}';
    }

    /**
     * Método compareTo para comparar a vida e dificuldade do jogador. compara a
     * dificuldade e pontos
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Player o) {

        if (this.grau.multiplicador() < o.grau.multiplicador()) {
            return 1;
        } else if (this.grau.multiplicador() == o.grau.multiplicador()) {
            if (o.vida == this.vida) {
                return 0;
            } else if (this.vida < o.vida) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}
