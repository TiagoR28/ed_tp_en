/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Colecoes.Graph;
import Colecoes.Queue;
import Colecoes.Stack;
import Colecoes.UnorderedList;
import exceptions.ElementNotFoundException;
import exceptions.EmptyCollectionException;
import java.util.Iterator;

/**
 *
 * @author 8130404 - Tiago Pacheco
 * @param <T>
 */
public class GrafoMapa<T> extends Graph<T> {

    private int adjMatrix[][];

    /**
     * Metodo construtor que instanceia a classe.
     *
     * @param adjMatrix
     */
    public GrafoMapa(int[][] adjMatrix) {
        this.adjMatrix = adjMatrix;
    }

    /**
     * Metodo construtor que instanceia a classe.
     */
    public GrafoMapa() {
        this.adjMatrix = new int[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
    }

    /**
     * Metodo para adicionar vertices ao grafo
     */
    @Override
    public void addVertex(T t) {

        if (numVertices == vertices.length) {
            this.expandCapacity();
        }
        vertices[numVertices] = t;
        for (int i = 0; i < vertices.length; i++) {
            adjMatrix[numVertices][i] = -1;
            adjMatrix[i][numVertices] = -1;
        }
        numVertices++;
    }

    /**
     * Metodo para expandir a capacidade do grafo
     */
    @Override
    protected void expandCapacity() {
        T[] tempVertices = (T[]) (new Object[vertices.length + 1]);
        int[][] tempAdjMatrix = new int[vertices.length + 1][vertices.length + 1];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                tempAdjMatrix[i][j] = adjMatrix[i][j];
            }
            tempVertices[i] = vertices[i];
        }
        vertices = tempVertices;
        adjMatrix = tempAdjMatrix;
    }

    /**
     * Metodo para adicionar ligações ao grafo
     *
     * @param t
     * @param t1
     * @param ligacao
     * @throws exceptions.ElementNotFoundException
     */
    public void addEdge(T t, T t1, int ligacao) throws ElementNotFoundException {
        addEdge(getIndex(t), getIndex(t1), ligacao);
    }

    /**
     * Metodo para adicionar ligações ao grafo
     *
     * @param index1
     * @param index2
     * @param ligacao
     */
    public void addEdge(int index1, int index2, int ligacao) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = ligacao;
        }
    }

    /**
     * Metodo iteratorDFS para percorrer o grafo
     *
     * @param t
     * @return
     * @throws exceptions.EmptyCollectionException
     */
    @Override
    public Iterator iteratorDFS(T t) throws EmptyCollectionException {
        Integer x = -1;
        boolean found;
        int startIndex = getIndex(t);
        Stack<Integer> traversalStack = new Stack<>();
        UnorderedList<T> resultList = new UnorderedList<>();
        Integer[] visited = new Integer[numVertices];
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        for (int i = 0; i < numVertices; i++) {
            visited[i] = -1;
        }

        traversalStack.push(startIndex);
        resultList.addToFront(vertices[startIndex]);
        visited[startIndex] = 0;

        while (!traversalStack.isEmpty()) {
            x = (Integer) traversalStack.peek();
            found = false;
            /**
             * Find a vertex adjacent to x that has not been visited and push it
             * on the stack
             */
            for (int i = 0; (i < numVertices) && !found; i++) {
                if (adjMatrix[x][i] != -1 && visited[i] == -1) {
                    traversalStack.push(i);
                    resultList.addToFront(vertices[i]);
                    visited[i] = 0;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        return resultList.iterator();
    }

    /**
     * Metodo para testar se o grafo é completo
     *
     * @param indice
     * @return String
     * @throws exceptions.EmptyCollectionException
     */
    public String testarGrafoCompleto(int indice) throws EmptyCollectionException {

        Iterator itr = iteratorDFS(vertices[indice]);
        int count = 0;

        try {
            while (itr.next() != null) {
                count++;
            }
        } catch (Exception c) {
        }

        if (count == vertices.length) {
            return "Grafo completo\n " + count;
        } else {
            return "Grafo imcompleto\n " + vertices.length;
        }
    }

    /**
     * Metodo iteratorShortestPath para percorrer o grafo pelo caminho mais
     * curto
     *
     * @param startIndex
     * @param targetIndex
     * @return
     * @throws exceptions.EmptyCollectionException
     */
    @Override
    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) throws EmptyCollectionException {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        Queue<Integer> traversalQueue = new Queue<>();
        UnorderedList<Integer> resultList = new UnorderedList<>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)
                || (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        Integer[] visited = new Integer[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = -1;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = 0;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            index = (traversalQueue.dequeue());

            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[index][i] != -1 && visited[i] == -1) {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;
                    traversalQueue.enqueue(i);
                    visited[i] = 0;
                }
            }
        }
        if (index != targetIndex) // Caso não tenha encontrado nenhum caminho
        {
            return resultList.iterator();
        }

        Stack<Integer> stack = new Stack<>();
        index = targetIndex;
        stack.push(index);
        do {
            index = predecessor[index];
            stack.push(index);
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToFront((stack.pop()));
        }

        return resultList.iterator();
    }

    /**
     * Metodo para apresentar o caminho mais curto entre dois aposentos
     *
     * @param indice1
     * @param indice2
     * @return string
     * @throws exceptions.EmptyCollectionException
     */
    public String caminhoMaisCurto(int indice1, int indice2) throws EmptyCollectionException {

        Iterator itr = iteratorShortestPath(vertices[indice1], vertices[indice2]);

        //Iterator itr = iteratorBFS();
        StringBuilder string = new StringBuilder();
        string.append("O caminho mais curto é:\n");
        while (itr.hasNext()) {
            Aposento p = (Aposento) itr.next();
            string.append(" -> ").append(p.getAposenAtual());
        }
        return string.toString();
    }

    /**
     * Metodo para verificar quais os aposentos que são alcançáveis a partir de
     * um determinado aposento
     *
     * @param indice
     * @return string
     * @throws exceptions.EmptyCollectionException
     */
    public String aposentosAlcancaveis(int indice) throws EmptyCollectionException {

        Iterator itr = iteratorDFS(vertices[indice]);
        UnorderedList<Aposento> apo = new UnorderedList();

        while (itr.hasNext()) {
            Aposento p = (Aposento) itr.next();
            if (!apo.contains(p)) {
                apo.addToFront(p);
            }
        }

        Iterator itr1 = apo.iterator();
        StringBuilder string = new StringBuilder();
        string.append("Os aposentos alcançaveis são:\n");
        while (itr1.hasNext()) {
            Aposento p = (Aposento) itr1.next();
            string.append("\n").append(p.getAposenAtual());
        }
        return string.toString();
    }

    /**
     * Metodo iteratorBFS para percorrer o grafo
     *
     * @param t
     * @return
     * @throws exceptions.EmptyCollectionException
     */
    @Override
    public Iterator iteratorBFS(T t) throws EmptyCollectionException {
        Integer x = -1;
        int startIndex = getIndex(t);
        Queue<Integer> traversalQueue = new Queue<>();
        UnorderedList<T> resultList = new UnorderedList<>();
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        int[] visited = new int[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = -1;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = 0;

        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToFront(vertices[x]);
            /**
             * Encontra todos os vertices adjacentes ao x que ainda não foram
             * vizitados e coloca-os na queue
             */
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[x][i] != -1 && visited[i] == -1) {
                    traversalQueue.enqueue(i);
                    visited[i] = 0;
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Metodo para aceder ao caminho menos custoso entre a posicao inicio e a do
     * fim desejada
     *
     * @param inicio
     * @param fim
     * @param player
     * @return caminho
     * @throws EmptyCollectionException
     * @throws ElementNotFoundException
     *
     * index.getAposenAtual().equals(inicio)
     */
    public Caminho caminhoMenosCustoso(String inicio, String fim, Player player) throws EmptyCollectionException, ElementNotFoundException {
        boolean[] visited = new boolean[vertices.length];
        UnorderedList<Integer> pathList = new UnorderedList<>();
        UnorderedList<String> lista = new UnorderedList<>();

        int comeco = -1;
        int termino = -1;
        //converte String para o index respetivo do vertice
        for (T vertice : vertices) {
            Aposento index = (Aposento) vertice;
            //System.out.println(index.getAposenAtual());
            if (index.getAposenAtual().equals(inicio)) {
                comeco = getIndex((T) index);

            }
            if (index.getAposenAtual().equals(fim)) {
                termino = getIndex((T) index);
            }
            if (comeco > -1 && termino > -1) {
                break;
            }
        }

        todosOsCaminhos(comeco, termino, visited, lista, pathList, comeco);

        Iterator itr = lista.iterator();
        String melhor = "";
        double contas = 0.0;
        int vezes = 0;
        while (itr.hasNext()) {
            String string = (String) itr.next();

            String[] split;
            split = string.split("->");
            String nova = "";
            int y = 0;
            int times = 0;
            double count = player.getVida();

            for (String split1 : split) {
                int i = Integer.parseInt(split1);

                Aposento p = (Aposento) vertices[i];
                count = count - p.getFantasma() * player.getGrau().multiplicador();

                if (y == 0) {
                    nova = p.getAposenAtual();
                    y++;
                } else {
                    nova = nova + "->" + p.getAposenAtual();
                }

                times++;

            }

            if (melhor.equals("")) {
                melhor = nova;
                contas = count;
                vezes = times;
            } else if (contas < count) {
                melhor = nova;
                contas = count;
                vezes = times;
            } else if (contas == count && vezes > times) {
                melhor = nova;
                contas = count;
                vezes = times;
            }
        }
        Caminho caminho = new Caminho(melhor, contas);
        return caminho;
    }

    /**
     * Metodo que corre para encontrar todos os possiveis caminho entre o o
     * ponto inicial atual e o final objectivo
     *
     * @param atual
     * @param objectivo
     * @param isVisited
     * @param lista
     * @param localPathList
     * @param inicial
     * @throws EmptyCollectionException
     * @throws ElementNotFoundException
     */
    public void todosOsCaminhos(Integer atual, Integer objectivo, boolean[] isVisited, UnorderedList<String> lista, UnorderedList<Integer> localPathList, int inicial) throws EmptyCollectionException, ElementNotFoundException {
        isVisited[atual] = true;
        if (atual.equals(objectivo)) {
            StringBuilder string = new StringBuilder();
            string.append(Integer.toString(inicial));
            Iterator itr = localPathList.iterator();
            while (itr.hasNext()) {
                Integer i = (Integer) itr.next();
                string.append("->").append(Integer.toString(i));
            }
            lista.addToRear(string.toString());
            isVisited[atual] = false;
            return;
        } else {
            Aposento p = (Aposento) vertices[atual];
            Iterator itr = p.getLigacoes().iterator();
            while (itr.hasNext()) {
                Aposento p1 = (Aposento) itr.next();
                if (!isVisited[getIndex((T) p1)]) {
                    localPathList.addToRear(getIndex((T) p1));
                    todosOsCaminhos(getIndex((T) p1), objectivo, isVisited, lista, localPathList, inicial);
                    localPathList.remove(getIndex((T) p1));
                }
            }

        }
        isVisited[atual] = false;
    }
}
