/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Colecoes.Graph;
import Colecoes.UnorderedList;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Mapa {

    private String nome;
    private int pontos;
    private String caminho;

    /**
     * metodo construtor que instancia a classe
     *
     * @param nome
     * @param pontos
     */
    public Mapa(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
    }

    /**
     * metodo getter para nome
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     * metodo stter para nome
     *
     * @return
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * metodo getter para pontos
     *
     * @return
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * metodo setter para pontos
     *
     * @return
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    /**
     * metodo getter para caminho
     *
     * @return
     */
    public String getCaminho() {
        return caminho;
    }

    /**
     * metodo setter para caminho
     *
     * @return
     */
    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

}
