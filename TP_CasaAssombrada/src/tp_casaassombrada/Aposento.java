/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Colecoes.UnorderedList;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Aposento {

    private String aposenAtual;
    private int fantasma;
    private UnorderedList<Aposento> ligacoes;

    /**
     * Metodo construtor que instanceia a classe.
     *
     * @param aposenAtual
     * @param fantasma
     */
    public Aposento(String aposenAtual, int fantasma) {
        this.aposenAtual = aposenAtual;
        this.fantasma = fantasma;
        this.ligacoes = new UnorderedList<>();
    }

    /**
     * Metodo construtor que instanceia apenas uma lista nao ordenada.
     */
    public Aposento() {
        this.ligacoes = new UnorderedList<Aposento>();
    }

    /**
     * Metodo getter para aposento atual
     *
     * @return
     */
    public String getAposenAtual() {
        return aposenAtual;
    }

    /**
     * metodo setter para aposento atual
     *
     * @param aposenAtual
     */
    public void setAposenAtual(String aposenAtual) {
        this.aposenAtual = aposenAtual;
    }

    /**
     * metodo getter para fantasma
     *
     * @return
     */
    public int getFantasma() {
        return fantasma;
    }

    /**
     * metodo setter para fantasma
     *
     * @param fantasma
     */
    public void setFantasma(int fantasma) {
        this.fantasma = fantasma;
    }

    /**
     * metodo getter para lista ligacoes
     *
     * @return
     */
    public UnorderedList<Aposento> getLigacoes() {
        return ligacoes;
    }

    /**
     * metodo setter para lista ligacoes
     *
     * @param ligacoes
     */
    public void setLigacoes(UnorderedList<Aposento> ligacoes) {
        this.ligacoes = ligacoes;
    }

    /**
     * metodo que adiciona objetos a lista.
     *
     * @param a
     */
    public void addLigação(Aposento a) {
        this.ligacoes.addToRear(a);
    }

}
