/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class Caminho {

    private String caminho;
    private double custo;

    /**
     * metodo que instancia a classe
     *
     * @param caminho
     * @param custo
     */
    public Caminho(String caminho, double custo) {
        this.caminho = caminho;
        this.custo = custo;
    }

    /**
     * metodo getter para caminho
     *
     * @return
     */
    public String getCaminho() {
        return caminho;
    }

    /**
     * metodo setter para caminho
     *
     * @param caminho
     */
    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    /**
     * metodo getter para custo
     *
     * @return
     */
    public double getCusto() {
        return custo;
    }

    /**
     * metodo setter para custo
     *
     * @param custo
     */
    public void setCusto(double custo) {
        this.custo = custo;
    }

}
