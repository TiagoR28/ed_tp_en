package tp_casaassombrada;

import Frames.Inicio;
import exceptions.ElementNotFoundException;
import exceptions.EmptyCollectionException;
import java.io.IOException;
import org.json.simple.parser.ParseException;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class TP_CasaAssombrada {

    /**
     * @param args the command line arguments
     * @throws org.json.simple.parser.ParseException
     * @throws java.io.IOException
     * @throws exceptions.ElementNotFoundException
     * @throws exceptions.EmptyCollectionException
     */
    public static void main(String[] args) throws ParseException, IOException, ElementNotFoundException, EmptyCollectionException {

        //incializacao das frames que levam ao jogo
        Inicio inicio = new Inicio(new String[]{"recursos/mapas/mapa.json", "recursos/mapas/mapa2.json"});
        inicio.setVisible(true);
    }
}
