/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_casaassombrada;

import Colecoes.OrderedList;
import Colecoes.UnorderedList;
import Enums.Dificuldade;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author 8130404 - Tiago Pacheco
 */
public class LoadJson {

    private UnorderedList<Aposento> listLiga;
    private JSONArray jsonObjctApo;

    public LoadJson() {

    }

    /**
     * metodo construtor que instancia a classe
     *
     * @param listLiga
     * @param jsonObjctApo
     */
    public LoadJson(UnorderedList<Aposento> listLiga, JSONArray jsonObjctApo) {
        this.listLiga = listLiga;
        this.jsonObjctApo = jsonObjctApo;
        this.getAposento();
        this.getCaminhos();
    }

    /**
     * Metodo que percorre o JSON e imprime o mapa e todas as ligacoes dos
     * aposentos
     *
     * @param jsonObjct
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    public String printMapa(JSONObject jsonObjct) throws FileNotFoundException, IOException, ParseException {

        String caminhos = "";

        String nome = (String) jsonObjct.get("nome");

        caminhos = caminhos + "\nNome: " + nome + "\n";

        JSONArray jsonMapa = (JSONArray) jsonObjct.get("mapa");
        JSONArray jsonLig = null;

        for (int i = 0; i < jsonMapa.size(); i++) {
            JSONObject jsonApo = (JSONObject) jsonMapa.get(i);
            caminhos = caminhos + "Area: " + (String) jsonApo.get("aposento");

            jsonLig = (JSONArray) jsonApo.get("ligacoes");

            for (int y = 0; y < jsonLig.size(); y++) {
                String s = (String) jsonLig.get(y);
                caminhos = caminhos + " --> " + s;
            }
            caminhos = caminhos + "\n";
        }
        return caminhos;
    }

    /**
     * metodo que le o JSON e junta a uma lista de aposentos o aposento e se
     * existe fantasma ou nao
     */
    private void getAposento() {
        //fantasma e aposento atual
        for (int i = 0; i < jsonObjctApo.size(); i++) {
            JSONObject jsonOnbj = (JSONObject) jsonObjctApo.get(i);
            String aposento = (String) jsonOnbj.get("aposento");
            int fantasma = Integer.parseInt((String) jsonOnbj.get("fantasma").toString());

            Aposento apo = new Aposento(aposento, fantasma);

            listLiga.addToRear(apo);
        }

    }

    /**
     * Metodo que corre a lista de aposentos e o JSON e cria as ligacoes entre
     * eles
     */
    private void getCaminhos() {
        //ligacoes
        //UnorderedList<Aposento> listfinal = new UnorderedList<>();

        Iterator itr1 = listLiga.iterator();

        for (int i = 0; i < jsonObjctApo.size(); i++) {
            Aposento apo = (Aposento) itr1.next();
            JSONObject jsonOnbj = (JSONObject) jsonObjctApo.get(i);
            JSONArray jsonArray = (JSONArray) jsonOnbj.get("ligacoes");

            for (int y = jsonArray.size() - 1; y >= 0; y--) {
                String conectado = (String) jsonArray.get(y);

                if (conectado.equals("entrada")) {
                    Aposento a = new Aposento("entrada", 0);
                    a.addLigação(apo);
                    apo.addLigação(a);
                    listLiga.addToRear(a);

                } else if (conectado.equals("exterior")) {
                    boolean b = false;
                    Iterator itr3 = listLiga.iterator();

                    while (itr3.hasNext()) {
                        Aposento p1 = (Aposento) itr3.next();
                        if (p1.getAposenAtual().equals("exterior")) {
                            b = true;
                            p1.addLigação(apo);
                            apo.addLigação(p1);
                            break;
                        }
                    }
                    if (!b) {
                        Aposento a = new Aposento("exterior", 0);
                        a.addLigação(apo);
                        apo.addLigação(a);
                        listLiga.addToRear(a);
                    }

                } else {
                    Iterator itr2 = listLiga.iterator();
                    while (itr2.hasNext()) {
                        Aposento p1 = (Aposento) itr2.next();
                        if (p1.getAposenAtual().equals(conectado)) {
                            apo.addLigação(p1);
                            break;
                        }
                    }
                }

            }
        }
    }

    /**
     * metodo que grava em ficheiro os resultados do jodo manual ou simulacao
     * criando o ficheiro caso nao exista e caso exista copia os dados para uma
     * lista ordenada e ordena por deficuldade e pontos de decrescente
     *
     * @param mapa
     * @param player
     * @param caminho
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    public void save(Mapa mapa, Player player, String caminho) throws FileNotFoundException, IOException, ParseException {
        //String caminho = "recursos/SAVE " + mapa.getNome() + ".json";

        //Criação do JSON para o resultado atual
        JSONObject historico = new JSONObject();
        historico.put("Jogado", player.getNome());
        historico.put("Mapa", mapa.getNome());
        historico.put("Dificuldade", player.getGrau().multiplicador());
        historico.put("PontosRestantes", player.getVida());
        historico.put("Caminho", mapa.getCaminho());

        //Criaçã odo array que vai guardar todos os rsultados
        JSONArray jogo = new JSONArray();

        //Aceder ao ficheiro que vai guardar os resultados
        File f = new File(caminho);
        FileWriter file;

        if (!f.exists()) {
            jogo.add(historico);
            f.createNewFile();
        } else {
            //Criação da lista Ordenada
            OrderedList<Historico> list = new OrderedList<>();
            //String jogador, String mapa, Dificuldade dificuldade, int pontosRestantes
            list.add(new Historico(player.getNome(), mapa.getNome(), player.getGrau(), player.getVida(), mapa.getCaminho()));

            Object obj1 = new JSONParser().parse(new FileReader(caminho));
            JSONArray jsonArray = (JSONArray) obj1;

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                int a = Integer.parseInt((String) jsonObj.get("PontosRestantes").toString()) + 0;
                int b = Integer.parseInt((String) jsonObj.get("Dificuldade").toString()) + 0;
                list.add(new Historico((String) jsonObj.get("Jogado"), (String) jsonObj.get("Mapa"), Dificuldade.BASICO.determinar(b), a, (String) jsonObj.get("Caminho")));
            }

            Iterator it = list.iterator();
            Historico h;
            while (it.hasNext()) {
                h = (Historico) it.next();

                JSONObject adicionar = new JSONObject();
                adicionar.put("Jogado", h.getJogador());
                adicionar.put("Mapa", h.getMapa());
                adicionar.put("Dificuldade", h.getDificuldade().multiplicador());
                adicionar.put("PontosRestantes", h.getPontosRestantes());
                adicionar.put("Caminho", h.getCaminho());

                jogo.add(adicionar);
            }
            f.createNewFile();
        }

        file = new FileWriter(caminho);
        file.write(jogo.toJSONString());
        file.flush();
        file.close();
    }
}
